This Is a simple template for a dockerized Rails 6 program. Modified from this https://docs.docker.com/samples/rails/


## Usage
Rename README
```sh
$ mv README.md README1.md
```

Initialize Project
```sh
$ docker-compose run --no-deps app rails new . --force --database=postgresql
```

Change Owner
```sh
$ sudo chown -R $USER:$USER .
```

Rename README
```sh
$ mv README1.md README.md
```

Update config/datbase.yml
```yml
default: &default
  adapter: postgresql
  encoding: unicode
  host: db
  username: postgres
  password: password
  pool: 5

development:
  <<: *default
  database: myapp_development


test:
  <<: *default
  database: myapp_test
```

Rebuild
```sh
$ docker-compose build
```

Install Webpacker
```sh
$ docker-compose run app rails webpacker:install
```

Boot App
```sh
$ docker-compose up
```

Initialize DB
```sh
$ docker-compose run app rake db:create
```